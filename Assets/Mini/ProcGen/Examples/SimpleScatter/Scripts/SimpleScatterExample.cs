using System.Collections;
using System.Collections.Generic;
using Mini.ProcGen;
using UnityEngine;


namespace Mini.Procgen.Examples
{
    public class SimpleScatterExample : MonoBehaviour
    {
        #region Public Fields
        #endregion Public Fields

        #region Protected Fields
        #endregion Protected Fields

        #region Private Fields
        [SerializeField]
        private GameObject _prefab = null;
        [SerializeField]
        private Transform _parent = null;
        [SerializeField]
        private float _visualizationSpeed = 0.1f;

        [Space(10)]
        [SerializeField, Range(2, 19)]
        private int _gridWidth = 19;
        [SerializeField, Range(2, 19)]
        private int _gridHeight = 19;
        [SerializeField, Range(0.0f, 1.0f)]
        private float _coverPercentage = 0.5f;
        [SerializeField]
        private int _samples = 10;

        private List<GameObject> _pointsList = new List<GameObject>();
        private Coroutine _scatterRoutine = null;
        #endregion Private Fields

        #region Unity Callbacks
        private void Start()
        {
            _scatterRoutine = StartCoroutine(ScatterRoutine());
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.R))
            {
                if (this._scatterRoutine != null)
                {
                    StopCoroutine(this._scatterRoutine);
                }

                while (this._pointsList.Count != 0)
                {
                    GameObject point = this._pointsList[0];
                    this._pointsList.RemoveAt(0);
                    point.SetActive(false);
                    Destroy(point);
                }

                _scatterRoutine = StartCoroutine(ScatterRoutine());
            }
        }
        #endregion Unity Callbacks

        #region Public Methods
        #endregion Public Methods

        #region Protected Methods
        #endregion Protected Methods

        #region Private Methods
        private IEnumerator ScatterRoutine()
        {
            List<Vector2Int> points = SimpleScatter.ScatterOnGrid(this._gridWidth, this._gridHeight, this._coverPercentage, this._samples);
            WaitForSecondsRealtime wait = new WaitForSecondsRealtime(this._visualizationSpeed);

            foreach (Vector2Int point in points)
            {
                GameObject pointGO = Instantiate(this._prefab, this._parent, false);
                pointGO.transform.position = CalculatePointGameObjectPosition(point);
                this._pointsList.Add(pointGO);

                yield return wait;
            }

            this._scatterRoutine = null;
        }

        private Vector3 CalculatePointGameObjectPosition(Vector2Int point)
        {
            return new Vector3(-((this._gridWidth - 1) / 2f * 0.5f) + point.x * 0.5f, 0, -((this._gridHeight - 1) / 2f * 0.5f) + point.y * 0.5f);
        }
        #endregion Private Methods
    }
}
