using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;


namespace Mini.ProcGen
{
    public static class SimpleScatter
    {
        #region Public Fields
        #endregion Public Fields

        #region Protected Fields
        #endregion Protected Fields

        #region Private Fields
        private const string ARGUMENT_EXCEPTION_FORMAT = "{0} has to be grater than zero!";
        #endregion Private Fields

        #region Public Methods
        public static List<Vector2Int> ScatterOnGrid(int width, int height, float coverPercentage, int samples = 10)
        {
            if (coverPercentage <= 0.0f)
            {
                throw new ArgumentException(string.Format(ARGUMENT_EXCEPTION_FORMAT, "Cover Percentage"));
            }
            if (width <= 0)
            {
                throw new ArgumentException(string.Format(ARGUMENT_EXCEPTION_FORMAT, "Grid Width"));
            }
            if (height <= 0)
            {
                throw new ArgumentException(string.Format(ARGUMENT_EXCEPTION_FORMAT, "Grid Height"));
            }
            if (samples <= 0)
            {
                throw new ArgumentException(string.Format(ARGUMENT_EXCEPTION_FORMAT, "Samples Count"));
            }
            
            bool [,] grid = new bool[width, height];
            List<Vector2Int> points = new List<Vector2Int>();
            int targetPointsCount = Mathf.CeilToInt(width * height * coverPercentage);
            Vector2Int lastPoint = new Vector2Int(Mathf.CeilToInt(width/2f), Mathf.CeilToInt(height/2f));
            int samplesCount = 0;

            while (targetPointsCount != points.Count || samples > samplesCount)
            {
                List<Vector2Int> newPoints = new List<Vector2Int>();

                for (int i = 0; i < 10; i++)
                {
                    Vector2Int newPoint = GetRandomPointPosition(width, height);

                    if (grid[newPoint.x, newPoint.y] == false)
                    {
                        newPoints.Add(newPoint);
                    }
                    else
                    {
                        samplesCount++;
                    }
                }
                
                Vector2Int point = FindValidPoint(lastPoint, newPoints);
                points.Add(point);
                grid[point.x, point.y] = true;
                lastPoint = point;
            }

            return points;
        }

        public static List<Vector2Int> ScatterOnGrid(int width, int height, float coverPercentage, List<Vector2Int> occupiedPoints, int samples = 10)
        {
            bool [,] grid = new bool[width, height];

            foreach (Vector2Int point in occupiedPoints)
            {
                grid[point.x, point.y] = true;
            }
            
            List<Vector2Int> points = new List<Vector2Int>();
            int targetPointsCount = Mathf.CeilToInt(width * height * coverPercentage);
            Vector2Int lastPoint = new Vector2Int(Mathf.CeilToInt(width/2f), Mathf.CeilToInt(height/2f));
            int samplesCount = 0;

            while (targetPointsCount >= points.Count || samples > samplesCount)
            {
                List<Vector2Int> newPoints = new List<Vector2Int>();

                for (int i = 0; i < 10; i++)
                {
                    Vector2Int newPoint = GetRandomPointPosition(width, height);

                    if (grid[newPoint.x, newPoint.y] == false)
                    {
                        newPoints.Add(newPoint);
                    }
                    else
                    {
                        samplesCount++;
                    }
                }
                
                if(newPoints.Count != 0)
                {
                    Vector2Int point = FindValidPoint(lastPoint, newPoints);
                    points.Add(point);
                    grid[point.x, point.y] = true;
                    lastPoint = point;
                }
                else
                {
                    samplesCount++;
                }
            }

            return points;
        }
        #endregion Public Methods

        #region Protected Methods
        #endregion Protected Methods

        #region Private Methods
        private static Vector2Int GetRandomPointPosition(int width, int height)
        {
            int x = Random.Range(0, width);
            int y = Random.Range(0, height);
            
            return new Vector2Int(x, y);
        }

        private static Vector2Int FindValidPoint(Vector2Int lastPoint, List<Vector2Int> candidates)
        {
            Vector2Int currentChoice = candidates[0];
            int distance = CalculateDistanceBetweenPointsOnGrid(currentChoice, lastPoint);

            for (int i = 1; i < candidates.Count; i++)
            {
                Vector2Int candidate = candidates[i];
                int dist = CalculateDistanceBetweenPointsOnGrid(candidate, lastPoint);

                if (dist > distance)
                {
                    distance = dist;
                    currentChoice = candidate;
                }
            }

            return currentChoice;
        }

        private static int CalculateDistanceBetweenPointsOnGrid(Vector2Int candidate, Vector2Int lastPoint)
        {
            int x = Mathf.Abs(candidate.x - lastPoint.x);
            int y = Mathf.Abs(candidate.y - lastPoint.y);

            return x + y;
        }
        #endregion Private Methods
    }
}
